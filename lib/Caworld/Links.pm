package Caworld::Links;
use Mojo::Base -strict, -signatures;
use Mojo::URL;
use Moose;
use namespace::autoclean;
no if ( $] >= 5.018 ), 'warnings' => 'experimental';
use Data::Printer;

has in_out_links => (
  is      => 'rw',
  isa     => 'HashRef[ArrayRef]'
);

sub BUILD {
    my $self = shift;
    $self->in_out_links( {inside => [], outside => []} );
    p $self->in_out_links;
}

=head2 build_in_out_links( $self, $links, $moo_url )

Return an hash ref of I<inside> or I<outside> arrays containing the links
discovered in the URL.

Take a C<Mojo::Collection> and the base URL Moose object as arguments.

=cut
sub build_in_out_links( $self, $links, $moo_url ) {

  foreach my $link ( $links->each ) {

    my $host = Mojo::URL->new( $link )->host;
    my $path = Mojo::URL->new( $link )->path;

    # We procede to the normalization of the URL in case it don't have a host
    # before it's path (it's a relative path)
    # TODO could be put in a separate function
    if ( not $host ) {
      $host = Mojo::URL
        ->new
        ->scheme( $moo_url->scheme )
        ->host( $moo_url->host )
        ->to_string;
      $link =  $host . $link;
    }

    # We procede to the normalization of the URL path in case it's path don't start
    # by /
    # TODO could be put in a separate function
    if ( $path and $path =~ m|^[a-z]+| ) {
      $link = Mojo::URL
        ->new
        ->scheme( $moo_url->scheme )
        ->host( $moo_url->host )
        ->path( $path )
        ->to_string;
    }

    if ( $moo_url->is_domain_insider( $link ) ) {
      push @{$self->in_out_links->{inside}}, $link;
    } else {
      push @{$self->in_out_links->{outside}}, $link;
    }
  }

  return $self->in_out_links;
}

__PACKAGE__->meta->make_immutable;

1;
