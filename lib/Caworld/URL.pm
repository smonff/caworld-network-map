package Caworld::URL;
use Mojo::Base -strict, -signatures;
use Mojo::URL;
use Mojo::JSON qw(true);
use Moose;
use namespace::autoclean;
no if ( $] >= 5.018 ), 'warnings' => 'experimental';
use Data::Printer;

=head1 REFACTORING

Some elements could be shared between Link.pm and URL.pm and they could lay in a Moose::Role.

=cut

has url => (
  is      => 'rw',
  isa     => 'Str',
);

has scheme => (
  is   => 'ro',
  isa  => 'Str',
  lazy => 1,
  builder => '_build_scheme'
);

has host => (
  is => 'ro',
  isa => 'Str',
  lazy => 1,
  builder => '_build_host'
);

sub BUILD {
  my $self = shift;
  if ( $self->url !~ m|/$| ) {
    if ( Mojo::URL->new( $self->url )->path eq undef ) {
      $self->url( $self->url . '/' );
    }
  }
  p $self->url;
}

=head2 is_domain_insider( $link, $host )

Return true if one of the links found in the page handled by C<Mojo::URL->new(
$url )->host> match the URL passed as a parameter with the C<--url> option.

=cut
sub is_domain_insider( $self, $link ) {
  my $host = Mojo::URL->new( $self->url )->host;
  return true if $link  =~ /$host/;
}

sub normalize_end( $self ) {
  if ( $self->url !~ m|/$| ) {
    say 'normalize';
    $self->url( $self->url . '/' );
  }
}

sub _build_scheme( $self ) {
  return Mojo::URL->new( $self->url )->scheme;
}

sub _build_host( $self ) {
  Mojo::URL->new( $self->url )->host;
}

__PACKAGE__->meta->make_immutable;

1;
