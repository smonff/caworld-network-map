#!/usr/bin/env perl
use Mojo::Base -strict, -signatures;
use Mojo::UserAgent;
use Mojo::DOM;
use Getopt::Long '2.50';
use lib 'lib';
use Data::Printer;

use Caworld::URL;
use Caworld::Links;

=encoding utf8

=head1 NAME

art.pl - A link searcher using a Mojo::UserAgent web client

=head1 SYNOPSIS

  perl -Mojo art.pl --url https://hi.balik.network
  perl art.pl --help

=head1 DESCRIPTION

Command line tool that accept an URL as a parameter and return a list of links
found on that webpage. The links are sorted in two categories: I<insiders> and
I<outsiders>.

=cut

my $url = '';
GetOptions( 'url=s' => \$url );

unless ( $url =~ m^https?://^ ) {
    die "A --url option is mandatory or it miss a protocol"
};

my $moo_url = Caworld::URL->new( { url => $url } );
p $moo_url;

my $tx = Mojo::UserAgent->new->get( $moo_url->url );

unless( $tx->result->is_success ) {
  die "There were a problem $!";
}

# TODO should later be moved to a separate module
my $dom = Mojo::DOM->new(  $tx->result->body );
my $links = $dom
  ->find( 'a' )
  ->map( attr => 'href' );

p $links;

my $moo_sorted_links = Caworld::Links
  ->new
  ->build_in_out_links( $links, $moo_url);

p $moo_sorted_links;

=head1 METHODS



=head1 ACKNOWLEDGEMENTS

Most of the things used in this program are inspired from the examples included
in brian d foy I<Mojolicious Web Client Programming> book and got written as a
training during reading the book.

=head1 AUTHOR

Sébastien Feugère, C<<sebastien@feugere.net>>

=head1 COPYRIGHT & LICENSE

Copyright 2019 Sébastien Feugère.

This program is free software; you can redistribute it and/or modify it under
the terms of the Artistic License v2.0.

See http://www.perlfoundation.org/artistic_license_2_0 or the LICENSE.md file
that comes with the ack distribution.

=cut

1;
